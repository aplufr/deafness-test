#!/bin/bash

set -e
function usage ()
{

	echo 
	echo '>>>'
	echo "Usage:	$0 freqmin step freqmax"
	echo "	ex: $0 500 500 10000"
	echo "Create a set of files with a uniq sine of frequence between freqmin and freqmax."
	echo "Files are created randomly so you can't guess what frequence is comming."
	echo '<<<'
	echo 
	exit
}

if [ $# -eq 3 ] 
then

	re="^[0-9]+$"
	echo -n "Check freqmin isnumber.. "
	[[ $1 =~ $re ]] && freq=$1 || usage
	echo pass
	echo -n "Check step isnumber.. "
	[[ $2 =~ $re ]] && step=$2 || usage
	echo pass
	echo -n "Check freqmax isnumber.. "
	[[ $3 =~ $re ]] && freqmax=$3 || usage
	echo pass
	echo -n "Check freqmin > 0 .. "
	[ $freq -gt 0 ] || usage
	echo pass
	echo -n "Check freqmin < freqmax .. "
	[ $freq -lt $freqmax ] || usage
	echo pass
	echo -n "Check freqmin+step < freqmax .. "
	[ $(($freq + $step)) -le $freqmax ] || usage
	echo pass
	echo -n "Check sox software.. "
	type sox &>/dev/null || { echo 'missing!' && exit ; }
	echo pass
	echo 'All basic test done.'
else
	usage
fi

declare -a freqs

#number a files to create
len=$(( ($freqmax - $freq  ) / $step  ))
#len=$(($(seq $freq $step $freqmax | wc -l) - 1))

rm -f $(pwd)/freq*.wav

echo "Range: from ${freq}Hz to $(($len * $step + $freq))Hz by ${step}Hz step."
echo "$(($len+1)) files will be created."
read -p "Press enter to continue."
echo 
#a tab whith all frequency
for i in `seq 0 $len`
do
	freqs[$i]=$freq
	let "freq = freq + step"
done


declare -a random

#randomize values...
i=0
while [  ${#random[@]}  -le ${len}  ]
do
	ran=$(($RANDOM % $(($len + 1 ))))
	[ -z  "${random[$ran]}" ] && random[$ran]=${freqs[$((i++))]}
done

#output files
echo "filename;frequence"> results.csv
echo '#EXTM3U' > all.m3u
for i in `seq 0 $len`
do
	echo "Generating sample file #$i"
	fname=$(printf freq%04d.wav $i)
	#yes, 1004300ms, 3 loops, at 48kHz produce 1min file length. Don't try to find why.
#	tones -c 1 -loop 3 -f -w $fname -s 48000 -16 :1004300 ${random[$i]}
	sox -n -b 16 -c 2 -e si -r 48k $fname synth 60 sine ${random[$i]}

	echo  "$fname;${random[$i]}" >> results.csv
	echo  "#EXTINF:31,$fname" >> all.m3u
	echo  "$(pwd)/$fname" >> all.m3u

done

echo "All files created. Use 'all.m3u' for read all files."
echo "Use results.csv to match frequence and files name."
