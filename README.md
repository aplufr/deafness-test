deafness-test
=============

A crappy deafness test for Linux.

Coded in bash but you need more software to use it...


Common requirement 
-------------------
	
* sox (on Debian/Ubuntu package sox)
* bash (it's a bash script...)

Recommended for both
-------------------------

* a spreadsheet (LibreOffice)
* a sound card (a good sound card, maybe headphones too).

Recommended for audiogen
-------------------------

* VLC (or a media-player able to read m3u playlist)


In detail
=========

Audiotest
----------

This shell script can be used to do an deafness-test.

You choose the frequency range (from, to) and the step, then all you have to do
is to listen carefully and answer if you heard something on your right or left
or nothing.

The script play each frequency on left or right and increase the gain until you
catch it. 

Once you have listened all frequency, a CSV file is created with your result.  
You can use a spreadsheet, like LibreOffice, to create a graph.

For more serious, frequency are shuffled.
For even more serious, don't answer left because the last time was right.. 

Results are from -90 to 0 (or nothing), -90 mean you ear very well, 0 very bad
 and nothing... (do I need to explain it?).

Audiogen
----------

This shell script create:
* a set of (mono) audio wav files using the software _sox_,
* a playlist for reading all,
* a CSV file with the file name and the frequency.

Each WAV files contains a unique sinus frequency.

The frequency are from *freqmin* to *freqmax* with a step of *step*.
You have to set this three mandatory options on the command line (see USAGE
below).

You can so play all sound using a software like VLC, and headphones. 

I recommend you set the volume of right/left channel to 0 (alsa/pulseaudio
allow this).

Then play all songs and adjust the volume for only left or right (usually you
can select only left/right output). Don't send sound on both in the same time.  
Take a note for each files which level your need on right and left.  
When you've finished, open the file results.csv in a spreadsheet software, like
LibreOffice and type your value.  
You can sort them by frequency after and draw a line chart.

Now you see which frequency you don't ear at all.

Important notice
==================

Please note, this isn't official and you can't say:  
\- "I've -52dB of loss starting 5kHz"

For two reason: 
- There is no way to calibrate the output level
- There is no way to be sure your sound card or your headphones are good.

Well.. you can use an oscilloscope to calibrate and check your sound card..

Usage
=====

Commonly you need to do test from 100Hz to 9000Hz, a step of 500Hz is a good idea.

bash ./audiogen.sh *freqmin* *step* *freqmax*

For example: 
* bash ./audiogen.sh 500 500 20000

   will create 40 files from 500Hz to 20kHz.
* bash ./audiogen.sh 500 200 10000

   will create 48 files from 500Hz to 9900Hz (because 10kHz can't be reach from
   500Hz with a step of 200Hz).

bas ./audiotest.sh *freqmin* *step* *freqmax*

For example:
* bash ./audiotest.sh 500 500 20000

  will make you a very long test.. 40 frequency played 4 seconds at least 80 
  times and probably more, as this depend on your ear :-).
  
  
Keywords
=========

deafness test, hearing loss, hearing impairment.
