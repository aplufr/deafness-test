#!/bin/bash

set -e
function usage ()
{

	echo 
	echo '>>>'
	echo "Usage:	$0"
	echo " Test with F=125, 250, 500, 1000, 2000, 4000, 8000"
	echo '<<<'
	echo 
	exit
}

if [ $# -ne 1 ]
then

	echo '[INFO] Running test'
	echo -n "Check sox software.. "
	type play &>/dev/null || { echo 'missing!' && exit ; }
	echo pass
	echo '[INFO] All test passed.'
	echo
	echo
else
	usage
fi

declare -a freqs

#number a files to create
#len=$(( ($freqmax - $freq  ) / $step  ))
#len=$(($(seq $freq $step $freqmax | wc -l) - 1))
dur=4

echo 
#echo "Range: from ${freq}Hz to $(($len * $step + $freq))Hz by ${step}Hz step, $((len*2)) tests."
#a tab whith all frequency
#for i in `seq 0 $len`
#do
#	freqs[$i]=$freq
#	let "freq = freq + step"
#done

freqs[0]=125
freqs[1]=250
freqs[2]=500
freqs[3]=1000
freqs[4]=2000
freqs[5]=4000
freqs[6]=8000
freqs[7]=10000
len=${#freqs[@]}

declare -a random

#randomize values...
i=0
while [  ${#random[@]}  -lt ${len}  ]
do
	ran=$(($RANDOM % $(($len)) ))
	[ -z  "${random[$ran]}" ] && random[$ran]=${freqs[$((i++))]}
done

#output files
duration=10
echo "frequence;left;right"> results.csv
echo 
echo "We are now playing for ${duration} seconds a noise song, please adjust your volume during this ${duration} seconds"
echo "The noise must be not too loud, not to quiet."
echo "Do not change your volume after!"
read -p "Press enter when ready."
ref=-45
echo -n "Now playing…"
play -n synth ${duration} sine 100 sine 500 sine 3000 sine 6000 square 100 square 500 square 3000 square 6000 channels 1 gain $ref 2>/dev/null
echo " Ok"
echo 
echo "You are going to hear each freq $dur seconds, on left and right with various gain."
echo "Important: Do not change adjust your volume now!"
echo "After each playing, you must answer by 'R' (Right), 'L' (Left) or 'N' (Nothing/Next) depending where you heard the signal."
echo "You may use 'P' (Previous) to go decrease volume (do the oposite as N)."
echo 
read -p "Press enter to start."
sample=0
cpt=0
for i in `seq 0 $len`
do
#based on previous system, but this one do a test!

	#all the gain
	declare -a allgain=( `seq -90 5 0` )

	l=$(($RANDOM % 2))
	r=$((($l+1)%2))

	ok=0
	j=0
	lg=''
	rg=''
	while [ $ok -eq 0 ]  && [ $j -lt ${#allgain[@]} ]
	do
		gain=${allgain[$j]}
		echo -e "\tPlaying frequency number $cpt/$((${#random[@]}*2)). Test number $((sample++)) (output gain: ${gain}dB)"

		play -q -n synth $dur sine ${random[$i]}  sine ${random[$i]} gain $gain  channels 2 remix $l $r 
		
		#echo "[DEBUG] Was, ${random[$i]}Hz gain $gain r=$r l=$l"
		until [ "x$ans" == "xl" ] || [ "x$ans" == "xr" ] || [ "x$ans" == "xn" ] || [ "x$ans" == "xp" ]
		do
			read -N 1 -p '-> Please press L, R, P or N: ' ans
			ans=$(tr [A-Z] [a-z] <<< "${ans}")
			echo 
		done
		if [ $l -eq 1 ] && [ $ans == l ] 
		then 
			lg=$gain
			l=0;r=1
			j=0;let "cpt+=1"
		elif [ $r -eq 1 ] && [ $ans == r ] 
		then
			rg=$gain
			l=1;r=0
			j=0;let "cpt+=1"
		elif [ $ans == p ]
		then
			if [ $j -gt 0 ] 
			then
				let j-=1
			else
				echo "Nope, not going backward already at lower gain"
				let j+=1
			fi

		else
			let j+=1
		fi
		[ -n "$rg" ] && [ -n "$lg" ]  && ok=1
	done
	echo "${random[$i]};$lg;$rg" >> results.csv
done

echo ";;;;numberoftest;$sample" >> results.csv
echo ";;;;referencegain;$ref" >> results.csv
echo "All done, open results.csv for your results."
